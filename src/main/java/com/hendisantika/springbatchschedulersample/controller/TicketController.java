package com.hendisantika.springbatchschedulersample.controller;

import com.hendisantika.springbatchschedulersample.model.Ticket;
import com.hendisantika.springbatchschedulersample.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-batch-scheduler-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-01
 * Time: 17:07
 * To change this template use File | Settings | File Templates.
 */
@RestController
public class TicketController {

    @Autowired
    private TicketRepository ticketRepository;

    @PostMapping("/bookTickets")
    public String bookTickets(@RequestBody List<Ticket> tickets) {
        ticketRepository.saveAll(tickets);
        return tickets.size() + " ticket booked successfully...";
    }

    @GetMapping("/getAllTickets")
    public List<Ticket> getAllTickets() {
        return ticketRepository.findAll();
    }
}