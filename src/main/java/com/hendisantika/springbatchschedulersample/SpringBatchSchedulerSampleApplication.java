package com.hendisantika.springbatchschedulersample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBatchSchedulerSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBatchSchedulerSampleApplication.class, args);
    }
}
