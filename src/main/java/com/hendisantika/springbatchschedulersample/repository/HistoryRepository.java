package com.hendisantika.springbatchschedulersample.repository;

import com.hendisantika.springbatchschedulersample.model.History;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-batch-scheduler-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-01
 * Time: 17:02
 * To change this template use File | Settings | File Templates.
 */
public interface HistoryRepository extends JpaRepository<History, Integer> {
}
